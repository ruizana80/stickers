import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    images:[
      {
        id:1,
        name:'Rick Morty',
        picture:require('../assets/Cartoon.jpeg')
      },
      {
        id:2,
        name:'Cartoons',
        picture:require('../assets/Cartoon.jpeg')
      },
      {
        id:3,
        name:'Dragon Ball',
        picture:require('../assets/Dragon-ball.jpeg')
      },
      {
        id:4,
        name:'Harry Potter',
        picture:require('../assets/Harry-potter.jpeg')
      },
      {
        id:5,
        name:'Marcas Plateadas',
        picture:require('../assets/Marcas.jpeg')
      },
      {
        id:6,
        name:'Nasa',
        picture:require('../assets/Nasa.jpeg')
      },
      {
        id:7,
        name:'Reflejantes',
        picture:require('../assets/Reflect.jpeg')
      },
      {
        id:8,
        name:'Simpsons',
        picture:require('../assets/Simpson.jpeg')
      },
      {
        id:9,
        name:'Skull',
        picture:require('../assets/Skull.jpeg')
      },
      {
        id:10,
        name:'Tenis',
        picture:require('../assets/Tenis.jpeg')
      }
    ],
  },
  mutations: {},
  actions: {},
  modules: {}
});
